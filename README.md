
## OREDATA ASSESSMENT

### Description

It is an assessment project prepared for Oredata. With this project, a user registration and login process is done. CRUD operations are used. User authorization operations were performed with RBAC. Task management system is provided with permissions. URL protection was realized with JWT.

## Project Information

A project where users can register and login. When registering, the user enters username, password and email information. When logging in, the user enters username and password information. Username and password information should not be less than 3 characters. Username and password information should not be empty. Email information must be in email format. Logged in users can see the tasks. Again, users can add boards and tasks themselves.

```
Username : Must not be less than 2 characters.
Password : Must not be less than 4 characters.
Email : Must be in email format.
```


## Installation

Clone the project.

```bash
git clone https://gitlab.com/ozkanahmetcan9705/oredata-assessment.git

```

After cloning the project, you can stand up the project by running the following commands.

```bash

cd Backend

npm install

npm start

```

```bash	

cd Frontend

npm install

npm start

```

Note: If you have a problem with dependencies, you can run the following command.

```bash

npm install --legacy-peer-deps

```	



## Technologies Used

```bash

***Frontend***
- React
- React Router
- Axios
- MUI
- Iconify
- Formik
- Auth Context

***Backend***
- Node.js
- Express
- JWT
- Nodemon

***Database***
- MongoDB

```	



## Project Screenshots
<h5> Sign In </h5>
<img src="/screenshots/1.jpg" width="500">
<h5> Sign Up </h5>
<img src="/screenshots/2.jpg" width="500">
<h5> Forgot Password </h5>
<img src="/screenshots/3.jpg" width="500">
<h5> Dashboard </h5>
<img src="/screenshots/4.jpg" width="500">
<h5> Add Board </h5>
<img src="/screenshots/5.jpg" width="500">
<h5> Add Task </h5>
<img src="/screenshots/6.jpg" width="500">
<h5> 404 </h5>
<img src="/screenshots/7.jpg" width="500">


