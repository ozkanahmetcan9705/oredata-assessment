const config = {
    mediaUrl: 'http://localhost:3000/images/', //if exists it can be use for getting images
    BACKEND_URL: 'http://localhost:3002/api/',
    BACKEND_BASEPATH: 'http://localhost:8000/',
    FRONTEND_DASHBOARD: 'http://localhost:3000/dashboard/',
    FRONTEND_BASEPATH: 'http://localhost:3000'
};
export default config;
