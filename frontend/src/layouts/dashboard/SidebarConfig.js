import { useState, useEffect } from 'react';
import axios from 'axios';
import { Icon } from '@iconify/react';
import dot2 from '@iconify/icons-bi/dot';
import  useAuth  from '../../hooks/useAuth';
import URL from '../../config';

const SidebarConfig = () => {
  const { user } = useAuth();
  const [board, setBoard] = useState([]);
  const [guestBoard, setGuestBoard] = useState([]);

  useEffect(() => {
    const getBoard = async () => {
      try {
        let res;
        if (user._id) {
          res = await axios.get(`${URL.BACKEND_URL}boards/all`, {
            headers: {
              id: user._id,
            },
          });
        } else {
          res = await axios.get(`${URL.BACKEND_URL}boards/all`, {
            headers: {
              id: user.id,
            },
          });
        }
        const boardsArray = res.data.boards || [];
        const guestArray = res.data.guestBoards || [];
        setBoard(boardsArray);
        setGuestBoard(guestArray);
      } catch (error) {
        console.log(error);
      }
    };
    getBoard();
  }, [user]);


  const generateBoardMenuItems = (boards) =>
      boards.map((board) => ({
        title: board.title,
        path: `/dashboard/board/${board._id}`,
        icon: <Icon icon={dot2} />,
      }));

  const navConfig = [
    {
      subheader: 'Kullanıcı Panoları',
      items: [...generateBoardMenuItems(board)],
    },
    {
      subheader: 'Misafir Panoları',
      items: [...generateBoardMenuItems(guestBoard)],
    },
  ];

  return navConfig;
};

export default SidebarConfig;
