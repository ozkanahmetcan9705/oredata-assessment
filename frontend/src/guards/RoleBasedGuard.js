import PropTypes from 'prop-types';
import { Container, Alert, AlertTitle } from '@material-ui/core';
import useAuth from '../hooks/useAuth';

RoleBasedGuard.propTypes = {
    accessibleRoles: PropTypes.string, // Example ['owner', 'guests']
    children: PropTypes.node
};

const useCurrentRole = () => {
    const { user } = useAuth();
    let role;
    if (user.role === 0) {
        role = 'User';
    }
    if (user.role === 1) {
        role = 'Admin1';
    }
    if (user.role === 2) {
        role = 'Admin2';
    }
    // 0-user, 1-admin1, 2-admin2

    return role;
};

export default function RoleBasedGuard({ accessibleRoles, children }) {
    const currentRole = useCurrentRole();
    if (!accessibleRoles.includes(currentRole)) {
        return (
            <Container>
                <Alert severity="error">
                    <AlertTitle>İşlem reddedildi</AlertTitle>
                    Yetkiniz Yok
                </Alert>
            </Container>
        );
    }

    return <>{children}</>;
}
