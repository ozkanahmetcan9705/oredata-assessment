import jwtDecode from 'jwt-decode';
import { verify, sign } from 'jsonwebtoken';

const isValidToken = (token) => {
    if (!token) {
        return false;
    }
    const decoded = jwtDecode(token);
    return decoded;
};

const setSession = (token) => {
    if (token) {
        localStorage.setItem('token', token);
    } else {
        localStorage.removeItem('token');
    }
};

export { isValidToken, setSession, verify, sign };
