//Listelerin Göründüğü Ekran
import { Container, Button, Grid, Modal, Box, Typography, TextField, Select, MenuItem, InputLabel, FormControl } from '@material-ui/core';
import { Icon } from '@iconify/react';
import { useEffect, useState } from 'react';
import plusCircle from '@iconify/icons-eva/plus-circle-outline';
import settings from '@iconify/icons-eva/settings-outline';
import trash from '@iconify/icons-eva/trash-outline';
import { useNavigate, useParams } from 'react-router-dom';
import Page from '../components/Page';
import useSettings from '../hooks/useSettings';
import useAuth from '../hooks/useAuth';
import HeaderBreadcrumbs from '../components/HeaderBreadcrumbs';
import { PATH_DASHBOARD } from '../routes/paths';
import BoardDetail from '../components/userDashboard/BoardDetail';
import URL from '../config';
import axios from "axios";
import { v4 as uuidv4 } from 'uuid';

export default function BoardDetails() {
    const { themeStretch } = useSettings();
    const [detail, setDetail] = useState(null);
    const [columnDetail,setColumnDetail] = useState(null);
    const [owner, setOwner] = useState('');
    const [users, setUsers] = useState([]);
    const [openModal, setOpenModal] = useState(false);
    const [openSettings, setOpenSettings] = useState(false);
    const [titleInput, setTitleInput] = useState('');
    const [selectedUser, setSelectedUser] = useState('');
    const { user } = useAuth();
    const { boardId } = useParams();
    const generatedId = uuidv4();
    console.log(boardId, "1 boardID in details")
    useEffect(() => {
        const boardDetail = async () => {
            try {
                let res;
                if(user._id){
                     res = await axios.get(`${URL.BACKEND_URL}boards/board/${boardId}`, {
                        headers: {
                            id: user._id
                        }
                    });
                } else {
                    res = await axios.get(`${URL.BACKEND_URL}boards/board/${boardId}`, {
                        headers: {
                            id: user.id
                        }
                    });
                }

                if (res.data.details && typeof res.data.details === 'object') {
                    setDetail([res.data.details]);
                } else {
                    setDetail([]);
                }
            } catch (error) {
                console.log(error);
            }
        };
        const columnDetail = async () => {
            try {
                let res;
                if (user._id) {
                    res = await axios.get(`${URL.BACKEND_URL}columns/all/${boardId}`, {
                        headers: {
                            id: user._id
                        }
                    });
                } else {
                    res = await axios.get(`${URL.BACKEND_URL}columns/all/${boardId}`, {
                        headers: {
                            id: user.id
                        }
                    });
                }

                if (res.data.columns) {
                    setColumnDetail(res.data.columns);
                } else {
                    setColumnDetail([]);
                }
            } catch (error) {
                console.log(error);
            }
        };

        boardDetail();
        columnDetail();
    }, [boardId, user._id]);

    const handleOpenModal = () => {
        setOpenModal(true);
    };
    const handleCloseModal = () => {
        setOpenModal(false);
    };
    const handleOpenSettingsModal = () => {
        setOpenSettings(true);
    };
    const handleCloseSettingsModal = () => {
        setOpenSettings(false);
    };
    const handleModalSubmit = async () => {
        const data = {
            title: titleInput,
            columnId: generatedId,
            boardId
        };
        let res;
        if (user._id){
             res = await axios.post(`${URL.BACKEND_URL}columns/add`, data, {
                headers: {
                    id: user._id
                }
            });
        } else {
            res = await axios.post(`${URL.BACKEND_URL}columns/add`, data, {
                headers: {
                    id: user.id
                }
            });
        }

        console.log(res, "res for add board");
        handleCloseModal();
        location.reload();
    };

    const handleSettingsModalSubmit = async () => {
        const data = {
            title: titleInput,
            boardId
        };
        let res;
        if (user._id) {
            res = await axios.put(`${URL.BACKEND_URL}boards/boardupdate`, data, {
                headers: {
                    id: user._id
                }
            });
        } else {
            res = await axios.put(`${URL.BACKEND_URL}boards/boardupdate`, data, {
                headers: {
                    id: user.id
                }
            });
        }
        handleCloseSettingsModal();
    };

    const handleAddUserModalSubmit = async () => {
        try {
            if (!selectedUser) {
                console.error("No user selected");
                return;
            }

            const data = {
                boardId,
                guestId: selectedUser, // Use selected user instead of user._id
            };
            let res;
            if (user._id){
                res = await axios.post(`${URL.BACKEND_URL}boards/addguest`, data, {
                    headers: {
                        id: user._id,
                    },
                });
            } else {
                res = await axios.post(`${URL.BACKEND_URL}boards/addguest`, data, {
                    headers: {
                        id: user.id,
                    },
                });
            }


            console.log(res, "res for add board");
            handleCloseSettingsModal();
        } catch (error) {
            console.error(error);
        }
    };

    const handleBoardDelete = async () => {
        const data = {
            boardId
        };
        let res;
        if (user._id){
            res = await axios.post(`${URL.BACKEND_URL}boards/delete`, data, {
                headers: {
                    id: user._id
                }
            });
        } else {
            res = await axios.post(`${URL.BACKEND_URL}boards/delete`, data, {
                headers: {
                    id: user.id
                }
            });
        }
    };


    useEffect(() => {
        const getUser = async () => {
            try {
                let res;
                if (user._id) {
                    res = await axios.get(`${URL.BACKEND_URL}user/find`, {
                        headers: {
                            id: user._id
                        }
                    });
                } else {
                    res = await axios.get(`${URL.BACKEND_URL}user/find`, {
                        headers: {
                            id: user.id
                        }
                    });
                }
                setOwner(res.data.user);
            } catch (error) {
                console.log(error);
            }
        };
        const getAllUsers = async () => {
            try {
                const res = await axios.get(`${URL.BACKEND_URL}user/findAll`);
                console.log(res.data, "all users in fetching");
                setUsers(res.data); // Set users as an array directly
            } catch (error) {
                console.log(error);
            }
        };
        getUser();
        getAllUsers();
    }, []);
    return (
        <Page title="Oredata | Ana sayfa">
            <Container maxWidth={themeStretch ? false : 'lg'}>
                {detail && detail.length > 0 && (
                    <HeaderBreadcrumbs
                        heading={detail[0].title}
                        links={[
                            { name: 'Gösterge Paneli', href: PATH_DASHBOARD.boards },
                            { name: 'Kullanıcı Panosu' }
                        ]}
                        owner={owner.username}
                        action={
                            <>
                                <Button
                                    variant="contained"
                                    startIcon={<Icon icon={plusCircle} width={25} height={25} />}
                                    onClick={handleOpenModal}
                                    sx={{mr: 1}}
                                >
                                    Liste Ekle
                                </Button>
                                <Button
                                    variant="contained"
                                    startIcon={<Icon icon={settings} width={25} height={25} />}
                                    onClick={handleOpenSettingsModal}
                                    sx={{mr: 1}}
                                >
                                    Panoyu Düzenle
                                </Button>
                                <Button
                                    variant="contained"
                                    startIcon={<Icon icon={trash} width={25} height={25} />}
                                    onClick={handleBoardDelete}
                                >
                                    Panoyu Sil
                                </Button>
                                <Modal
                                    open={openModal}
                                    onClose={handleCloseModal}
                                    aria-labelledby="modal-title"
                                    aria-describedby="modal-description"
                                >
                                    <Box
                                        sx={{
                                            position: 'absolute',
                                            top: '50%',
                                            left: '50%',
                                            transform: 'translate(-50%, -50%)',
                                            width: 400,
                                            bgcolor: 'background.paper',
                                            boxShadow: 24,
                                            p: 4,
                                        }}
                                    >
                                        <Typography id="modal-title" variant="h6" component="h2" gutterBottom>
                                            Başlık Ekle
                                        </Typography>
                                        <TextField
                                            label="Başlık"
                                            variant="outlined"
                                            fullWidth
                                            value={titleInput}
                                            onChange={(e) => setTitleInput(e.target.value)}
                                        />
                                        <Box sx={{
                                            mt: 2,
                                            display: 'flex',
                                            justifyContent: 'flex-end'
                                        }}>
                                            <Button onClick={handleCloseModal}>
                                                Cancel
                                            </Button>
                                            <Button variant="contained" onClick={handleModalSubmit}>
                                                Submit
                                            </Button>
                                        </Box>
                                    </Box>
                                </Modal>
                                <Modal
                                    open={openSettings}
                                    onClose={handleCloseSettingsModal}
                                    aria-labelledby="modal-title"
                                    aria-describedby="modal-description"
                                >
                                    <Box
                                        sx={{
                                            position: 'absolute',
                                            top: '50%',
                                            left: '50%',
                                            transform: 'translate(-50%, -50%)',
                                            width: 400,
                                            bgcolor: 'background.paper',
                                            boxShadow: 24,
                                            p: 4,
                                        }}
                                    >
                                        <Typography id="modal-title" variant="h6" component="h2" gutterBottom>
                                            Panoyu Düzenle
                                        </Typography>
                                        <TextField
                                            label="Pano Başlığı"
                                            variant="outlined"
                                            fullWidth
                                            value={titleInput || (detail && detail.length > 0 ? detail[0].title : '')}
                                            onChange={(e) => setTitleInput(e.target.value)}
                                        />

                                        <Box sx={{
                                            mt: 2,
                                            display: 'flex',
                                            justifyContent: 'flex-end'
                                        }}>
                                            <Button variant="contained" onClick={handleSettingsModalSubmit}>
                                                Başlığı Kaydet
                                            </Button>
                                        </Box>
                                        <Typography id="modal-title" variant="h6" component="h2" gutterBottom sx={{ mt: 2 }}>
                                            Kullanıcı Ekle
                                        </Typography>
                                        <FormControl fullWidth variant="outlined">
                                            <InputLabel id="user-select-label">Select User</InputLabel>
                                            <Select
                                                labelId="user-select-label"
                                                id="user-select"
                                                value={selectedUser}
                                                onChange={(e) => setSelectedUser(e.target.value)}
                                                label="Select User"
                                            >
                                                {users.map((user) => (
                                                    <MenuItem key={user._id} value={user._id}>
                                                        {user.username}
                                                    </MenuItem>
                                                ))}
                                            </Select>
                                        </FormControl>
                                        <Box sx={{
                                            mt: 2,
                                            display: 'flex',
                                            justifyContent: 'flex-end'
                                        }}>
                                            <Button onClick={handleCloseSettingsModal}>
                                                İptal
                                            </Button>
                                            <Button variant="contained" onClick={handleAddUserModalSubmit}>
                                                Kullanıcı Ekle
                                            </Button>
                                        </Box>
                                    </Box>
                                </Modal>
                            </>
                        }

                    />

                )}
                <Grid container spacing={3} sx={{ justifyContent:  'left' }}>
                    {columnDetail?.map((columnDetail, i) => (
                        <Grid key={i} item xs={6} sm={4} md={3}>
                            <BoardDetail columnDetail={columnDetail} boardId={boardId} />
                        </Grid>
                    ))}
                </Grid>
            </Container>
        </Page>
    );
}

