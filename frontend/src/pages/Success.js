import { motion } from 'framer-motion';
import { Link as RouterLink } from 'react-router-dom';
import { styled } from '@material-ui/core/styles';
import { Box, Button, Typography, Container } from '@material-ui/core';
import { MotionContainer, varBounceIn } from '../components/animate';
import Page from '../components/Page';
import SuccessApproved from '../assets/success_200';

const RootStyle = styled(Page)(({ theme }) => ({
  display: 'flex',
  minHeight: '100%',
  alignItems: 'center',
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10)
}));

export default function Success() {
  return (
    <RootStyle title="İzin Onaylama Başarılı! | VİK">
      <Container>
        <MotionContainer initial="initial" open>
          <Box sx={{ maxWidth: 480, margin: 'auto', textAlign: 'center' }}>
            <motion.div variants={varBounceIn}>
              <Typography variant="h3" paragraph>
                İşlem Başarılı!
              </Typography>
            </motion.div>
            <motion.div variants={varBounceIn}>
              <SuccessApproved sx={{ height: 260, my: { xs: 5, sm: 10 } }} />
            </motion.div>

            <Button to="/" size="large" variant="contained" component={RouterLink}>
              Gösterge Paneline Dön
            </Button>
          </Box>
        </MotionContainer>
      </Container>
    </RootStyle>
  );
}
