import { capitalCase } from 'change-case';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { styled } from '@material-ui/core/styles';
import {Box, Card, Container, Typography, Button, Stack, Link} from '@material-ui/core';
import { jwtDecode } from 'jwt-decode';
import axios from 'axios';
import { PATH_AUTH } from '../../routes/paths';
import useAuth from '../../hooks/useAuth';
import AuthLayout from '../../layouts/AuthLayout';
import Page from '../../components/Page';
import { MHidden } from '../../components/@material-extend';
import { LoginForm } from '../../components/authentication/login';
import Logo from '../../components/Logo';
import FL from '../../assets/first_layer.jpg';
import URL from '../../config';

const RootStyle = styled(Page)(({ theme }) => ({
  [theme.breakpoints.up('md')]: {
    display: 'cover'
  }
}));

const SectionStyle = styled(Card)(({ theme }) => ({
  width: '100%',
  maxWidth: 464,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  margin: theme.spacing(2, 0, 2, 2)
}));

const ContentStyle = styled('div')(({ theme }) => ({
  maxWidth: 480,
  margin: 'auto',
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: theme.spacing(12, 0)
}));
export default function Login() {
  const navigate = useNavigate();
  const { method, login, register } = useAuth();
  const handleLoginAuth0 = async () => {
    try {
      await login();
    } catch (error) {
      console.error(error);
    }
  };

  return (
      <RootStyle
          title="Oredata | Giriş Yap"
          style={{
            backgroundImage: `url(${FL})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            height: '100vh',
          }}
      >
      <Container>
        <ContentStyle>
          <MHidden width="mdDown">
            <Box sx={{ px: '35%', marginTop: '-10%' }}>
              <Logo />
            </Box>
            <Typography variant="h3" sx={{ marginTop: '5%', marginBottom: '1%', textAlign: 'center' }}>
              Oredata Task Management
            </Typography>
          </MHidden>
            <LoginForm />
          <Stack direction="row" spacing={2} sx={{ mt: 2, justifyContent: 'space-between' }}>
            <Typography sx={{ textAlign: 'left' }}>
              <Link underline="none" variant="subtitle2" component={RouterLink} to={PATH_AUTH.register}>
                Kaydol
              </Link>
            </Typography>
            <Typography sx={{ textAlign: 'right' }}>
              <Link underline="none" variant="subtitle2" component={RouterLink} to={PATH_AUTH.resetPassword}>
                Şifrenizi mi unuttunuz
              </Link>
            </Typography>
          </Stack>

        </ContentStyle>
      </Container>
    </RootStyle>
  );
}
