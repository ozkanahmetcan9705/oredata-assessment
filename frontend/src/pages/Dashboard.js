//Panoların sergilendiği yer
import React, { useEffect, useState } from 'react';
import { Box, Button, Container, Grid, OutlinedInput, InputAdornment, Typography, FormControlLabel, Radio, RadioGroup } from '@material-ui/core';
import { Icon } from '@iconify/react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import { styled } from '@material-ui/core/styles';
import { Skeleton } from '@material-ui/lab';
import { filter } from 'lodash';
import searchFill from '@iconify/icons-eva/search-fill';
import plusCircle from '@iconify/icons-eva/plus-circle-outline';
import axios from 'axios';
import Page from '../components/Page';
import useSettings from '../hooks/useSettings';
import useAuth from '../hooks/useAuth';
import HeaderBreadcrumbs from '../components/HeaderBreadcrumbs';
import SearchNotFound from '../components/SearchNotFound';
import { PATH_DASHBOARD } from '../routes/paths';
import BoardCards from '../components/userDashboard/BoardCards';
import URL from '../config';

const SkeletonLoad = (
    <>
        {[...Array(12)].map((_, index) => (
            <Grid item xs={6} sm={4} md={3} key={index}>
                <Skeleton variant="rectangular" width="100%" sx={{ paddingTop: '50%', borderRadius: 2 }} />
            </Grid>
        ))}
    </>
);

const SearchStyle = styled(OutlinedInput)(({ theme }) => ({
    width: 240,
    marginBottom: theme.spacing(5),
    transition: theme.transitions.create(['box-shadow', 'width'], {
        easing: theme.transitions.easing.easeInOut,
        duration: theme.transitions.duration.shorter,
    }),
    '&.Mui-focused': {
        width: 320,
        boxShadow: theme.customShadows.z8,
    },
    '& fieldset': {
        borderWidth: `1px !important`,
        borderColor: `${theme.palette.grey[500_32]} !important`,
    },
}));

function applyFilter(array, query) {
    return filter(array, (board) => {
        let matches = true;

        if (query) {
            const properties = ['title'];
            let containsQuery = false;
            properties.forEach((property) => {
                if (board[property] && board[property].toLowerCase().includes(query.toLowerCase())) {
                    containsQuery = true;
                }
            });
            if (!containsQuery) {
                matches = false;
            }
        }
        return matches;
    });
}

const TabsWrapperStyle = styled('div')(({ theme }) => ({
    zIndex: 9,
    bottom: 0,
    width: '100%',
    display: 'flex',
    position: 'absolute',
    backgroundColor: theme.palette.background.paper,
    [theme.breakpoints.up('sm')]: {
        justifyContent: 'center',
    },
    [theme.breakpoints.up('md')]: {
        justifyContent: 'flex-end',
        paddingRight: theme.spacing(3),
    },
}));

const Dashboard = () => {
    const { themeStretch } = useSettings();
    const [board, setBoard] = useState([]);
    const [guestBoard, setGuestBoard] = useState([]);
    const [isDialogOpen, setDialogOpen] = useState(false);
    const [sortOption, setSortOption] = useState('');
    const [sortModalOpen, setSortModalOpen] = useState(false);
    const [findBoard, setFindBoard] = useState('');
    const [loadingSkeleton, setLoadingSkeleton] = useState(true);
    const { user } = useAuth();
    const boardFiltered = applyFilter(board, findBoard);
    const guestBoardFiltered = applyFilter(guestBoard, findBoard);
    const isNotFound = boardFiltered.length === 0 && guestBoardFiltered.length === 0;
    const handleFindBoard = (event) => {
        setFindBoard(event.target.value);
    };

    useEffect(() => {
        const getBoard = async () => {
            try {
                let res;
                if(user._id){
                     res = await axios.get(`${URL.BACKEND_URL}boards/all`, {
                        headers: {
                            id: user._id,
                        },
                    });
                } else {
                     res = await axios.get(`${URL.BACKEND_URL}boards/all`, {
                        headers: {
                            id: user.id,
                        },
                    });
                }
                const boardsArray = res.data.boards || [];
                const guestBoardsArray = res.data.guestBoards || [];
                setBoard(boardsArray);
                setGuestBoard(guestBoardsArray);
                setTimeout(() => {
                    setLoadingSkeleton(false);
                }, 2500);
            } catch (error) {
                console.log(error);
            }
        };
        getBoard();
    }, [user.id, user._id]);
    const AddBoardDialog = ({ open, onClose }) => {
        const [title, setTitle] = useState('');

        const handleAddBoard = async () => {
            const data = {
                title,
                userId: user._id
            };
            const res = await axios.post(`${URL.BACKEND_URL}boards/add`, data
            );
            console.log(res, "res for add board");
            handleCloseDialog();
            location.reload();
        };

        const handleTitleChange = (event) => {
            setTitle(event.target.value);
        };

        return (
            <Dialog open={open} onClose={onClose} fullWidth>
                <DialogTitle>Yeni Pano Olustur</DialogTitle>
                <DialogContent>
                    <Typography variant="subtitle2" sx={{ mt: 2, mb: 3 }}>
                        Pano Adi
                    </Typography>
                    <OutlinedInput
                        fullWidth
                        placeholder="Pano Adi"
                        value={title}
                        onChange={handleTitleChange}
                        startAdornment={
                            <InputAdornment position="start">
                                <Icon icon={searchFill} />
                            </InputAdornment>
                        }
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={onClose}>Iptal</Button>
                    <Button onClick={handleAddBoard} variant="contained" sx={{ mx: 1 }}>
                        Olustur
                    </Button>
                </DialogActions>
            </Dialog>
        );
    };

    const sortByTitle = (boards) => {
        return boards.sort((board1, board2) =>
            board1.title.toLowerCase().localeCompare(board2.title.toLowerCase())
        );
    };

    const sortByCreatedAt = (boards) => {
        return boards.sort((board1, board2) => new Date(board1.createdAt) - new Date(board2.createdAt));
    };


    const handleOpenDialog = () => {
        setDialogOpen(true);
    };

    const handleCloseDialog = () => {
        setDialogOpen(false);
    };

    const handleOpenSortModal = () => {
        setSortModalOpen(true);
    };

    const handleCloseSortModal = () => {
        setSortModalOpen(false);
        let sortedBoards;
        switch (sortOption) {
            case 'title':
                sortedBoards = sortByTitle(board);
                break;
            case 'createdAt':
                sortedBoards = sortByCreatedAt(board);
                break;
            default:
                sortedBoards = board;
        }
        setBoard(sortedBoards);
    };

    const SortModal = ({ open, onClose }) => {
        return (
            <Dialog open={open} onClose={onClose} fullWidth>
                <DialogTitle>Sıralama</DialogTitle>
                <DialogContent>
                    <Typography variant="subtitle2" sx={{ mt: 2, mb: 3 }}>
                        Sıralama Seçenekleri
                    </Typography>
                    <RadioGroup
                        value={sortOption}
                        onChange={(event) => setSortOption(event.target.value)}
                    >
                        <FormControlLabel
                            value="title"
                            control={<Radio />}
                            label="A'dan Z'ye Göre"
                        />
                    </RadioGroup>
                </DialogContent>
                <DialogActions>
                    <Button onClick={onClose}>Iptal</Button>
                    <Button
                        onClick={() => {
                            handleCloseSortModal();
                        }}
                        variant="contained"
                        sx={{ mx: 1 }}
                    >
                        Sırala
                    </Button>
                </DialogActions>
            </Dialog>
        );
    };

    return (
        <Page title="Oredata | Ana sayfa">
            <Container maxWidth={themeStretch ? false : 'lg'}>
                <HeaderBreadcrumbs
                    heading="Panolarim"
                    links={[
                        { name: 'Gösterge Paneli', href: PATH_DASHBOARD.boards },
                        { name: 'Kullanici Panolari' },
                    ]}
                    action={
                        <React.Fragment>
                            <Button
                                variant="contained"
                                startIcon={<Icon icon={plusCircle} width={25} height={25} />}
                                onClick={handleOpenDialog}
                            >
                                Pano Ekle
                            </Button>
                            <Button
                                variant="contained"
                                sx={{ mx: 1 }}
                                onClick={handleOpenSortModal}
                            >
                                Sırala
                            </Button>
                            <SortModal open={sortModalOpen} onClose={handleCloseSortModal} />

                        </React.Fragment>

                    }
                />
                <AddBoardDialog open={isDialogOpen} onClose={handleCloseDialog} />
                <SearchStyle
                    value={findBoard}
                    onChange={handleFindBoard}
                    placeholder="Pano Arayin"
                    startAdornment={
                        <InputAdornment position="start">
                            <Box component={Icon} icon={searchFill} sx={{ color: 'text.disabled' }} />
                        </InputAdornment>
                    }
                />
                <Grid container spacing={3} sx={{ justifyContent: isNotFound && 'center' }}>
                    {!loadingSkeleton ? (
                        <>
                            <Grid container spacing={3} item xs={12} md={12} xl={12}>
                                <Grid item xs={12} md={12} xl={12}>
                                    <Typography id="modal-title" variant="h6" component="h2" gutterBottom xs={12} sm={12} md={12}>
                                        Kullanıcı Panoları
                                    </Typography>
                                </Grid>
                                {boardFiltered.map((board, i) => (
                                    <Grid key={i} item xs={6} sm={4} md={3} xl={3} sx={{ mt: 3 }}>
                                        <BoardCards board={board} />
                                    </Grid>
                                ))}
                            </Grid>
                            <Grid container spacing={3} item xs={12} md={12} xl={12}>
                                <Grid item xs={12} md={12} xl={12}>
                                    <Typography id="modal-title" variant="h6" component="h2" gutterBottom xs={12} sm={12} md={12} sx={{mt:2}}>
                                        Misafir Panoları
                                    </Typography>
                                </Grid>
                                {guestBoardFiltered.map((board, i) => (
                                    <Grid key={i} item xs={6} sm={4} md={3} sx={{mt:3}}>
                                        <BoardCards board={board} />
                                    </Grid>
                                ))}
                            </Grid>
                        </>
                    ) : (
                        SkeletonLoad
                    )}
                    {isNotFound && !loadingSkeleton && (
                        <Box sx={{ mt: 5 }}>
                            <SearchNotFound searchQuery={findBoard} />
                        </Box>
                    )}
                </Grid>
            </Container>
        </Page>
    );
};

export default Dashboard;
