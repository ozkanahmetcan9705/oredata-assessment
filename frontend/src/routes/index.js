import { Suspense, lazy } from 'react';
import { Navigate, useRoutes, useLocation } from 'react-router-dom';
import DashboardLayout from '../layouts/dashboard';
import LogoOnlyLayout from '../layouts/LogoOnlyLayout';
import LoadingScreen from '../components/LoadingScreen';
import GuestGuard from '../guards/GuestGuard';
import AuthGuard from '../guards/AuthGuard';
import RoleBasedGuard from '../guards/RoleBasedGuard';

const LoadingFallback = () => {
    const { pathname } = useLocation();
    const isDashboard = pathname.includes('/dashboard');

    return (
        <LoadingScreen
            sx={{
                ...(!isDashboard && {
                    top: 0,
                    left: 0,
                    width: 1,
                    zIndex: 9999,
                    position: 'fixed'
                })
            }}
        />
    );
};

const Loadable = (Component) => (props) => (
    <Suspense fallback={<LoadingFallback />}>
        <Component {...props} />
    </Suspense>
);

export default function Router() {
    return useRoutes([
        // Auth Routes
        {
            path: 'auth',
            children: [
                {
                    path: 'login',
                    element: (
                        <GuestGuard>
                            <Login />
                        </GuestGuard>
                    )
                },
                {
                    path: 'register',
                    element: (
                        <GuestGuard>
                            <Register />
                        </GuestGuard>
                    )
                },
                { path: 'login-unprotected', element: <Login /> },
                { path: 'register-unprotected', element: <Register /> }
            ]
        },
        {
            path: '/',
            element: (
                <AuthGuard>
                    <DashboardLayout />
                </AuthGuard>
            ),
            children: [
                { path: '/', element: <Navigate to="/dashboard/boards" replace /> },
                { path: '/dashboard/boards', element: <Dashboard /> },
                { path: '/dashboard/board/:boardId', element: <BoardDetails /> },
            ]
        },

        // Main Routes
        {
            path: '*',
            element: <LogoOnlyLayout />,
            children: [
                { path: '404', element: <NotFound /> },
                { path: 'success', element: <Success /> },
                { path: '*', element: <Navigate to="/404" replace /> }
            ]
        }
    ]);
}

// IMPORT COMPONENTS
const Login = Loadable(lazy(() => import('../pages/authentication/Login')));
const Register = Loadable(lazy(() => import('../pages/authentication/Register')));
const Dashboard = Loadable(lazy(() => import('../pages/Dashboard')));
const BoardDetails = Loadable(lazy(() => import('../pages/BoardDetails')));
const NotFound = Loadable(lazy(() => import('../pages/Page404')));
const Success = Loadable(lazy(() => import('../pages/Success')));
