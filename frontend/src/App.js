import Router from './routes';
import ThemeConfig from './theme';
import Settings from './components/settings';
import RtlLayout from './components/RtlLayout';
import ScrollToTop from './components/ScrollToTop';
import ThemePrimaryColor from './components/ThemePrimaryColor';
import NotistackProvider from './components/NotistackProvider';
import useAuth from './hooks/useAuth';
import LoadingScreen from './components/LoadingScreen';

export default function App() {
  const { isInitialized } = useAuth();
  return (
      <ThemeConfig>
        <ThemePrimaryColor>
            <RtlLayout>
              <NotistackProvider>
                <Settings />
                <ScrollToTop />
                {isInitialized ? <Router /> : <LoadingScreen />}
              </NotistackProvider>
            </RtlLayout>
        </ThemePrimaryColor>
      </ThemeConfig>
  );
}
