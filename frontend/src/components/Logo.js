//Logo
import PropTypes from 'prop-types';
import { styled } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import Banner from '../assets/Banner.png';

Logo.propTypes = {
    sx: PropTypes.object
};

const CoverImgStyle = styled('img')({
    zIndex: 8,
    width: '60',
    height: '50'
});

export default function Logo({ sx }) {
    return (
        <Box sx={{ width: 120, height: 90, ...sx }}>
            <CoverImgStyle alt="Oredata" src={Banner} />
        </Box>
    );
}
