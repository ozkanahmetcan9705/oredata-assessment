//Dashboard'daki Panoların Kart Yapıları
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { alpha, styled, useTheme } from '@material-ui/core/styles';
import { Box, Card, Avatar, Typography } from '@material-ui/core';
import SvgIconStyle from '../SvgIconStyle';
import * as React from 'react';
import {useEffect, useState} from "react";
import axios from "axios";
import URL from "../../config";

const CardMediaStyle = styled('div')(({ theme }) => ({
    display: 'flex',
    position: 'relative',
    justifyContent: 'center',
    paddingTop: 'calc(30% * 9 / 16)',
    backgroundColor: alpha(theme.palette.primary.darker, 0.72)
}));


BoardCards.propTypes = { //dashboarddan gelen board değeri
    board: PropTypes.object.isRequired
};

export default function BoardCards({ board, ...other }) {
    const { id, title } = board;
    const [owner, setOwner] = useState('')
    const user = board.user;
    const theme = useTheme();
    const navigate = useNavigate(); // Add this line to use the navigate function


    useEffect(() => {
        const getUser = async () => { //kullanıcıyı çeker
            try {
                const res = await axios.get(`${URL.BACKEND_URL}user/find`, {
                    headers : {
                        id: user
                    }
                });
                setOwner(res.data.user);
            } catch (error) {
                console.log(error);
            }
        };
        getUser();
    }, []);

    const handleBoardClick = () => { //özelleştirilmiş boarda yönlendirme
        let boardID = board._id
        const boardPath = `/dashboard/board/${boardID}`;
        navigate(boardPath);
    };
    return (
        <Card {...other} onClick={handleBoardClick}>
            <CardMediaStyle>
                <SvgIconStyle
                    color="paper"
                    src="/static/icons/shape-avatar.svg"
                    sx={{
                        width: 144,
                        height: 62,
                        zIndex: 10,
                        bottom: -26,
                        position: 'absolute'
                    }}
                />
                <Avatar
                    alt={title}
                    src="/static/icons/shield.svg"
                    sx={{
                        width: 64,
                        height: 64,
                        zIndex: 11,
                        position: 'absolute',
                        transform: 'translateY(-50%)'
                    }}
                />
            </CardMediaStyle>

            <Typography variant="subtitle1" align="center" sx={{ mt: 6 }}>
                {title}
            </Typography>
            <Typography variant="body2" align="center" sx={{ color: 'text.secondary' }}>
                {owner.username}
            </Typography>
            <Box sx={{ mb: 3 }} />
        </Card>
    );
}
