//Listelerin kart yapıları ve card task'a gönderilen değerlerin yer aldığı sayfa
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { Icon } from '@iconify/react';
import { styled, useTheme } from '@material-ui/core/styles';
import { Box, Card, Divider, Typography, Button, Modal, TextField, Grid, OutlinedInput } from '@material-ui/core';
import URL from "../../config";
import useAuth from "../../hooks/useAuth";
import { useEffect, useState } from 'react';
import axios from "axios";
import plusCircle from '@iconify/icons-eva/plus-circle-outline';
import CardTasks from './CardTasks';
import { v4 as uuidv4 } from 'uuid';

const ListCardStyle = styled(Card)(({ theme }) => ({
    backgroundColor: theme.palette.background.default,
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.customShadows.z8,
    cursor: 'pointer',
    display: 'flex', // Added to make cards start from the left side
    flexDirection: 'column', // Adjusted to make the content stack vertically
}));

const ListCardContentStyle = styled('div')(({ theme }) => ({
    position: 'relative',
    padding: theme.spacing(2),
}));

BoardDetail.propTypes = {
    columnDetail: PropTypes.object.isRequired,
    boardId: PropTypes.string.isRequired,
    moveCard: PropTypes.func,
};

const reorder = (column, draggedCardID, droppedCardID) => {
    const { cardIds } = column;

    console.log("Original CardIds:", cardIds);

    const filteredCardIds = cardIds.filter((id) => id !== null);

    console.log("Filtered CardIds:", filteredCardIds);

    const startIndex = filteredCardIds.indexOf(draggedCardID);
    const endIndex = filteredCardIds.indexOf(droppedCardID);

    console.log("Start Index:", startIndex);
    console.log("End Index:", endIndex);

    if (startIndex === -1 || endIndex === -1) {
        console.error("Invalid start or end index");
    }

    const result = [...cardIds];
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};


export default function BoardDetail({ columnDetail, boardId, ...other }) {
    const columnID = columnDetail._id;
    const { _id, title } = columnDetail;
    const [cards, setCards] = useState(null);
    const [openModal, setOpenModal] = useState(false);
    const [openCardDialog, setOpenCardDialog] = useState(false);
    const [updatedTitle, setUpdatedTitle] = useState('');
    const [description, setDescription] = useState("");
    const { user } = useAuth();
    const [titleInput, setTitleInput] = useState('');
    const generatedId = uuidv4();
    const [editMode, setEditMode] = useState(false);

    const handleCardMove = async (draggedCardID, droppedCardID) => {
        try {
            const data = {
                sameColumnId: columnID,
                samecolumnCardIds: reorder(columnDetail, draggedCardID, droppedCardID),
            };
            let res;
            if(user._id) {
                res = await axios.post(`${URL.BACKEND_URL}cards/reorder/samecolumn`, data, {
                    headers: {
                        id: user._id,
                    },
                });
            } else {
                res = await axios.post(`${URL.BACKEND_URL}cards/reorder/samecolumn`, data, {
                    headers: {
                        id: user.id,
                    },
                });
            }


            console.log(res.data.message);
            console.log(res.data.savedColumn);
            setCards((prevCards) => {
                const updatedCards = prevCards.map((card) => {
                    if (card.cardId === res.data.savedColumn._id) {
                        const cardIndex = res.data.savedColumn.cardIds.findIndex(
                            (id) => id === card.cardId
                        );

                        if (cardIndex !== -1) {
                            const updatedCardIds = [
                                res.data.savedColumn.cardIds[cardIndex],
                                ...res.data.savedColumn.cardIds.slice(0, cardIndex),
                                ...res.data.savedColumn.cardIds.slice(cardIndex + 1),
                            ];

                            const updatedCard = { ...card, cardIds: updatedCardIds };

                            return updatedCard;
                        }
                    }
                    return card;
                });

                return updatedCards;
            });
        } catch (error) {
            console.error('Card move error:', error);
        }
    };

    const handleEditClick = () => {
        setEditMode(!editMode);
    };

    const handleCardClick = () => {
        setOpenCardDialog(true);
    };

    const handleCloseCardDialog = () => {
        setOpenCardDialog(false);
    };

    const handleCardEditSubmit = async (cardId) => {
        const data = {
            title: updatedTitle,
            description: description,
            cardId
        };
        let res;
        if (user._id) {
            res = await axios.put(`${URL.BACKEND_URL}cards/update`, data, {
                headers: {
                    id: user._id
                }
            });
        } else {
            res = await axios.put(`${URL.BACKEND_URL}cards/update`, data, {
                headers: {
                    id: user.id
                }
            });
        }
        location.reload();
    };

    useEffect(() => {
        let isMounted = true;
        const allCards = async () => {
            try {
                const data = {
                    columnIds: columnID
                };
                let res;
                if(user._id) {
                    res = await axios.post(`${URL.BACKEND_URL}cards/getallcards`, data, {
                        headers: {
                            id: user._id
                        }
                    });
                } else {
                    res = await axios.post(`${URL.BACKEND_URL}cards/getallcards`, data, {
                        headers: {
                            id: user.id
                        }
                    });
                }

                if (isMounted) {
                    if (res.data.cards) {
                        setCards(res.data.cards);
                    } else {
                        setCards(null);
                    }
                }
            } catch (error) {
                console.log(error);
            }
        };

        allCards();

        return () => {
            isMounted = false;
        };
    }, [columnID, user._id]);
    const handleOpenModal = () => {
        setOpenModal(true);
    };
    const handleCloseModal = () => {
        setOpenModal(false);
    };
    const handleModalSubmit = async () => {
        const data = {
            title: titleInput,
            cardId: generatedId,
            columnID
        };
        let res;
        if(user._id){
            res = await axios.post(`${URL.BACKEND_URL}cards/add`, data, {
                headers: {
                    id: user._id
                }
            });
        } else {
            res = await axios.post(`${URL.BACKEND_URL}cards/add`, data, {
                headers: {
                    id: user.id
                }
            });
        }
        handleCloseModal();
        location.reload();
    };

    return (
        <ListCardStyle {...other}>
            <ListCardContentStyle>
                <Typography variant="subtitle1">
                    {title}
                </Typography>
                <Divider sx={{ my: 1, width: '100%' }} />
                <Grid container spacing={3} sx={{ justifyContent: 'center' }}>
                    {cards?.length > 0 ? (
                        cards?.map((card, i) => (
                            <Grid key={i} item xs={12} sm={12} md={12}>
                                <CardTasks cards={card} boardId={boardId} columnDetail={columnDetail} moveCard={handleCardMove} onClick={handleCardClick} />
                                {openCardDialog && (
                                    <Modal
                                        open={openCardDialog}
                                        onClose={handleCloseCardDialog}
                                        aria-labelledby="card-modal-title"
                                        aria-describedby="card-modal-description"
                                    >
                                        <Box
                                            sx={{
                                                position: 'absolute',
                                                top: '50%',
                                                left: '50%',
                                                transform: 'translate(-50%, -50%)',
                                                width: 400,
                                                bgcolor: 'background.paper',
                                                boxShadow: 24,
                                                p: 4,
                                                display: 'flex',
                                                flexDirection: 'column',
                                            }}
                                        >
                                            <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', mb: 2 }}>
                                                {editMode ? (<TextField
                                                        id="card-modal-title"
                                                        label="Başlık"
                                                        value={updatedTitle || (card && card.title ? card.title : '')}
                                                        variant="outlined"
                                                        onChange={(e) => setUpdatedTitle(e.target.value)}
                                                        sx={{ mb: 2 }}
                                                    />) :
                                                    (<Typography id="card-modal-title" variant="h6" component="h2">
                                                        {card.title}
                                                    </Typography>)}
                                                <Button sx={{ ml: 'auto' }}
                                                        onClick={handleEditClick}
                                                >
                                                    Düzenle
                                                </Button>
                                            </Box>
                                            <TextField
                                                id="card-modal-description"
                                                label="Açıklama"
                                                multiline
                                                rows={4}
                                                value={description || (card && card.description ? card.description : '')}
                                                onChange={(e) => setDescription(e.target.value)}
                                                variant="outlined"
                                                sx={{ mb: 2 }}
                                            />
                                            <Button sx={{ mt: 'auto' }}
                                                    variant="contained"
                                                    onClick={() => handleCardEditSubmit(card.cardId)}
                                            >
                                                Kaydet
                                            </Button>
                                        </Box>
                                    </Modal>
                                )}
                            </Grid>

                        ))
                    ) : (
                        <Typography sx={{mt: 3}}>Henüz Görev Eklenmedi</Typography>
                    )}
                </Grid>

                <Divider sx={{ my: 1, width: '100%' }} />
                <Button
                    variant="contained"
                    startIcon={<Icon icon={plusCircle} width={25} height={25} />}
                    onClick={handleOpenModal}
                    sx={{mt: 1}}
                >
                    Kart Ekle
                </Button>
                <Modal
                    open={openModal}
                    onClose={handleCloseModal}
                    aria-labelledby="modal-title"
                    aria-describedby="modal-description"
                >
                    <Box
                        sx={{
                            position: 'absolute',
                            top: '50%',
                            left: '50%',
                            transform: 'translate(-50%, -50%)',
                            width: 400,
                            bgcolor: 'background.paper',
                            boxShadow: 24,
                            p: 4,
                        }}
                    >
                        <Typography id="modal-title" variant="h6" component="h2" gutterBottom>
                            Görev Ekle
                        </Typography>
                        <TextField
                            label="Başlık"
                            variant="outlined"
                            fullWidth
                            value={titleInput}
                            onChange={(e) => setTitleInput(e.target.value)}
                        />
                        <Box sx={{
                            mt: 2,
                            display: 'flex',
                            justifyContent: 'flex-end'
                        }}>
                            <Button onClick={handleCloseModal}>
                                Cancel
                            </Button>
                            <Button variant="contained" onClick={handleModalSubmit}>
                                Submit
                            </Button>
                        </Box>
                    </Box>
                </Modal>
            </ListCardContentStyle>
        </ListCardStyle>
    );
}
