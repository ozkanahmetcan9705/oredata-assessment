//Görevlerin kart yapıları
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { styled, useTheme } from '@material-ui/core/styles';
import { Card, Typography, Button, Modal, TextField, Grid } from '@material-ui/core';
import useAuth from "../../hooks/useAuth";
import { useState } from 'react';

const ListCardStyle = styled(Card)(({ theme }) => ({
    backgroundColor: theme.palette.background.default,
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.customShadows.z8,
    cursor: 'pointer',
    display: 'flex', // Added to make cards start from the left side
    flexDirection: 'column', // Adjusted to make the content stack vertically
}));

const ListCardContentStyle = styled('div')(({ theme }) => ({
    position: 'relative',
    padding: theme.spacing(2),
}));

CardTasks.propTypes = {
    columnDetail: PropTypes.object.isRequired,
    boardId: PropTypes.string.isRequired,
    cards: PropTypes.object.isRequired,
    moveCard: PropTypes.func.isRequired,
};

export default function CardTasks({ cards, boardId, columnDetail, moveCard, ...other }) {
    const cardID = cards.cardId;
    const title = cards.title;
    const handleDragStart = (e) => {
        e.dataTransfer.setData('text/plain', cardID);
    };
    const handleDrop = (e) => {
        e.preventDefault();
        const draggedCardID = e.dataTransfer.getData('text/plain');
        if (draggedCardID !== cardID) {
            moveCard(draggedCardID, cardID);
        }
    };
    const handleDragOver = (e) => {
        e.preventDefault();
    };
    const handleDragEnd = (e) => {
        e.preventDefault();
    };
    return (
        <ListCardStyle draggable
                       onDragStart={handleDragStart}
                       onDrop={handleDrop}
                       onDragOver={handleDragOver}
                       onDragEnd={handleDragEnd} {...other}>
            <ListCardContentStyle>
                <Typography variant="subtitle1">
                    {title}
                </Typography>
            </ListCardContentStyle>
        </ListCardStyle>
    );
}
