import PropTypes from 'prop-types';
import { Paper, Typography } from '@material-ui/core';

SearchNotFound.propTypes = {
  searchQuery: PropTypes.string
};

export default function SearchNotFound({ searchQuery = '', ...other }) {
    console.log(searchQuery.length, "search length")
  return (
    <Paper {...other}>
      <Typography gutterBottom align="center" variant="subtitle1">
        Pano Bulunamadı
      </Typography>
        {
            searchQuery.length === 0 ? (
                <Typography variant="body2" align="center">
                    Henüz Pano Eklemediniz.
                </Typography>
            ) : (
                <Typography variant="body2" align="center">
                    <strong>&quot;{searchQuery}&quot;</strong> adında pano bulunamadı.
                </Typography>
            )
        }
    </Paper>
  );
}
