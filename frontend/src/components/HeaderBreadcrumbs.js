//Dashboardlarda gelen üst başlık yönlendirme ve dialog butonları
import { isString } from 'lodash';
import PropTypes from 'prop-types';
import { Box, Typography, Link } from '@material-ui/core';
import { MBreadcrumbs } from './@material-extend';

HeaderBreadcrumbs.propTypes = {
  links: PropTypes.array,
  action: PropTypes.node,
  heading: PropTypes.string.isRequired,
  owner: PropTypes.string,
  users: PropTypes.string,
  moreLink: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  sx: PropTypes.object
};

export default function HeaderBreadcrumbs({ links, action, heading, owner, users, moreLink = '' || [], sx, ...other }) {
  return (
    <Box sx={{ mb: 5, ...sx }}>
      <Box sx={{ display: 'flex', alignItems: 'center' }}>
        <Box sx={{ flexGrow: 1 }}>
          <Typography variant="h4" gutterBottom>
            {heading}
          </Typography>
            <Typography variant="body2" sx={{ color: 'text.secondary' }}>
            {owner && users && `${owner} / ${users}`}
            </Typography>
          <MBreadcrumbs links={links} {...other} />
        </Box>

        {action && (
            <Box sx={{ display: 'flex', alignItems: 'center', p: 1 }}>
              <Typography variant="body2" sx={{ color: 'text.secondary', mr: 1 }}>
                {owner && `${owner}`}
              </Typography>
              {action}
            </Box>
        )}
      </Box>

      <Box sx={{ mt: 2 }}>
        {isString(moreLink) ? (
          <Link href={moreLink} target="_blank" variant="body2">
            {moreLink}
          </Link>
        ) : (
          moreLink.map((href) => (
            <Link noWrap key={href} href={href} variant="body2" target="_blank" sx={{ display: 'table' }}>
              {href}
            </Link>
          ))
        )}
      </Box>
    </Box>
  );
}
