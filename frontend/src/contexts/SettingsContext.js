//Ayarları kontrol eden yer
import PropTypes from 'prop-types';
import { createContext } from 'react';
import useLocalStorage from '../hooks/useLocalStorage';
import palette from '../theme/palette';

const PRIMARY_COLOR = [
    // DEFAULT
    {
        name: 'default',
        ...palette.light.primary
    },
    // PURPLE
    {
        name: 'purple',
        lighter: '#F3E8FF',
        light: '#DFC4FF',
        main: '#C49FFF',
        dark: '#A076D2',
        darker: '#8264A6',
        contrastText: '#fff'
    },
    // CYAN
    {
        name: 'cyan',
        lighter: '#D6FFFF',
        light: '#A3F5FF',
        main: '#6ED5FF',
        dark: '#4AA8B7',
        darker: '#377A7A',
        contrastText: palette.light.grey[800]
    },
    // BLUE
    {
        name: 'blue',
        lighter: '#C4DFFF',
        light: '#9FB1FF',
        main: '#7B8CFF',
        dark: '#5866B7',
        darker: '#414F7A',
        contrastText: '#fff'
    },
    // ORANGE
    {
        name: 'orange',
        lighter: '#FFEED6',
        light: '#FFD6A3',
        main: '#FFB56E',
        dark: '#B78C47',
        darker: '#795C32',
        contrastText: palette.light.grey[800]
    },
    // RED
    {
        name: 'red',
        lighter: '#FFD6D1',
        light: '#FFA7A0',
        main: '#FF7771',
        dark: '#B74F4B',
        darker: '#793630',
        contrastText: '#fff'
    },
    // LIGHT PURPLE
    {
        name: 'light-purple',
        lighter: '#EFE2FF',
        light: '#D3AFFF',
        main: '#AD6EFF',
        dark: '#8453D2',
        darker: '#623BA6',
        contrastText: '#fff'
    },
    // GREEN
    {
        name: 'green',
        lighter: '#E6FFEB',
        light: '#ABFFC1',
        main: '#6DF591',
        dark: '#47B56C',
        darker: '#2E7F48',
        contrastText: palette.light.grey[800]
    },
    // YELLOW
    {
        name: 'yellow',
        lighter: '#FFFAD6',
        light: '#FFEB9F',
        main: '#FFD96B',
        dark: '#B78C3F',
        darker: '#795C29',
        contrastText: palette.light.grey[800]
    },
    // PINK
    {
        name: 'pink',
        lighter: '#FFDBDB',
        light: '#FFA9A9',
        main: '#FF7777',
        dark: '#B74B4B',
        darker: '#793232',
        contrastText: '#fff'
    },
    // TEAL
    {
        name: 'teal',
        lighter: '#D6FFFF',
        light: '#A3F5FF',
        main: '#6ED5FF',
        dark: '#4AA8B7',
        darker: '#377A7A',
        contrastText: palette.light.grey[800]
    },
    // LIME
    {
        name: 'lime',
        lighter: '#F6FFC9',
        light: '#D1FF86',
        main: '#9EEC31',
        dark: '#68B80A',
        darker: '#3D8100',
        contrastText: palette.light.grey[800]
    },
    // INDIGO
    {
        name: 'indigo',
        lighter: '#C4CDFF',
        light: '#879CFF',
        main: '#4D64FF',
        dark: '#2E3EB7',
        darker: '#1F297A',
        contrastText: '#fff'
    },
    // BROWN
    {
        name: 'brown',
        lighter: '#FFECDB',
        light: '#FFCBA8',
        main: '#FFA371',
        dark: '#B76147',
        darker: '#794A30',
        contrastText: palette.light.grey[800]
    },
    // GRAY
    {
        name: 'gray',
        lighter: '#F3F3F3',
        light: '#D8D8D8',
        main: '#A8A8A8',
        dark: '#7C7C7C',
        darker: '#545454',
        contrastText: '#fff'
    },
    // MINT
    {
        name: 'mint',
        lighter: '#E5FFF9',
        light: '#AFFFE6',
        main: '#7BFFD4',
        dark: '#4BF4C0',
        darker: '#28A993',
        contrastText: palette.light.grey[800]
    },
    // SAND
    {
        name: 'sand',
        lighter: '#FFF8D7',
        light: '#FFEBB3',
        main: '#FFD488',
        dark: '#B77A47',
        darker: '#79542E',
        contrastText: palette.light.grey[800]
    },
    // CORAL
    {
        name: 'coral',
        lighter: '#FFDAD9',
        light: '#FFA9A6',
        main: '#FF7874',
        dark: '#B7514E',
        darker: '#793130',
        contrastText: '#fff'
    }
];

SetColor.propTypes = {
    themeColor: PropTypes.oneOf([
        'default',
        'purple',
        'cyan',
        'blue',
        'orange',
        'red',
        'light-purple',
        'green',
        'yellow',
        'pink',
        'teal',
        'lime',
        'indigo',
        'brown',
        'gray',
        'mint',
        'sand',
        'coral'
    ])
};

function SetColor(themeColor) {
    let color;
    const DEFAULT = PRIMARY_COLOR[0];
    const PURPLE = PRIMARY_COLOR[1];
    const CYAN = PRIMARY_COLOR[2];
    const BLUE = PRIMARY_COLOR[3];
    const ORANGE = PRIMARY_COLOR[4];
    const RED = PRIMARY_COLOR[5];
    const LIGHTPURPLE = PRIMARY_COLOR[6];
    const GREEN = PRIMARY_COLOR[7];
    const YELLOW = PRIMARY_COLOR[8];
    const PINK = PRIMARY_COLOR[9];
    const TEAL = PRIMARY_COLOR[10];
    const LIME = PRIMARY_COLOR[11];
    const INDIGO = PRIMARY_COLOR[12];
    const BROWN = PRIMARY_COLOR[13];
    const GRAY = PRIMARY_COLOR[14];
    const MINT = PRIMARY_COLOR[15];
    const SAND = PRIMARY_COLOR[16];
    const CORAL = PRIMARY_COLOR[17];

    switch (themeColor) {
        case 'purple':
            color = PURPLE;
            break;
        case 'cyan':
            color = CYAN;
            break;
        case 'blue':
            color = BLUE;
            break;
        case 'orange':
            color = ORANGE;
            break;
        case 'red':
            color = RED;
            break;
        case 'light-purple':
            color = LIGHTPURPLE;
            break;
        case 'green':
            color = GREEN;
            break;
        case 'yellow':
            color = YELLOW;
            break;
        case 'pink':
            color = PINK;
            break;
        case 'teal':
            color = TEAL;
            break;
        case 'lime':
            color = LIME;
            break;
        case 'indigo':
            color = INDIGO;
            break;
        case 'brown':
            color = BROWN;
            break;
        case 'gray':
            color = GRAY;
            break;
        case 'mint':
            color = MINT;
            break;
        case 'sand':
            color = SAND;
            break;
        case 'coral':
            color = CORAL;
            break;
        default:
            color = DEFAULT;
    }
    return color;
}

const initialState = {
    themeMode: 'light',
    themeDirection: 'ltr',
    themeColor: 'default',
    themeStretch: false,
    onChangeMode: () => {},
    onChangeDirection: () => {},
    onChangeColor: () => {},
    onToggleStretch: () => {},
    setColor: PRIMARY_COLOR[0],
    colorOption: []
};

const SettingsContext = createContext(initialState);

SettingsProvider.propTypes = {
    children: PropTypes.node
};

function SettingsProvider({ children }) {
    const [settings, setSettings] = useLocalStorage('settings', {
        themeMode: initialState.themeMode,
        themeDirection: initialState.themeDirection,
        themeColor: initialState.themeColor,
        themeStretch: initialState.themeStretch
    });

    const onChangeMode = (event) => {
        setSettings({
            ...settings,
            themeMode: event.target.value
        });
    };

    const onChangeDirection = (event) => {
        setSettings({
            ...settings,
            themeDirection: event.target.value
        });
    };

    const onChangeColor = (event) => {
        setSettings({
            ...settings,
            themeColor: event.target.value
        });
    };

    const onToggleStretch = () => {
        setSettings({
            ...settings,
            themeStretch: !settings.themeStretch
        });
    };

    return (
        <SettingsContext.Provider
            value={{
                ...settings,
                // Mode
                onChangeMode,
                // Direction
                onChangeDirection,
                // Color
                onChangeColor,
                setColor: SetColor(settings.themeColor),
                colorOption: PRIMARY_COLOR.map((color) => ({
                    name: color.name,
                    value: color.main
                })),
                // Stretch
                onToggleStretch
            }}
        >
            {children}
        </SettingsContext.Provider>
    );
}

export { SettingsProvider, SettingsContext };
