//Kullanıcı işlemlerinin yapıldıgı context
import { createContext, useEffect, useReducer } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import jwtDecode from 'jwt-decode';
import { isValidToken, setSession } from '../utils/jwt';
import URL from '../config';

const initialState = {
    isAuthenticated: false,
    isInitialized: false,
    user: null
};

const handlers = {
    INITIALIZE: (state, action) => {
        const { isAuthenticated, user } = action.payload;
        return {
            ...state,
            isAuthenticated,
            isInitialized: true,
            user
        };
    },
    LOGIN: (state, action) => {
        const { user } = action.payload;
        return {
            ...state,
            isAuthenticated: true,
            user
        };
    },
    LOGOUT: (state) => ({
        ...state,
        isAuthenticated: false,
        user: null
    }),
    REGISTER: (state, action) => {
        const { user } = action.payload;

        return {
            ...state,
            isAuthenticated: true,
            user
        };
    }
};

const reducer = (state, action) => (handlers[action.type] ? handlers[action.type](state, action) : state);

const AuthContext = createContext({
    ...initialState,
    method: 'local',
    login: () => Promise.resolve(),
    logout: () => Promise.resolve()
});

AuthProvider.propTypes = {
    children: PropTypes.node
};

function AuthProvider({ children }) {
    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        const initialize = async () => {
            try {
                const token = window.localStorage.getItem('token');
                if (token && isValidToken(token)) {
                    const decode = jwtDecode(token);
                    let response;
                    const id = decode.sub;
                    await axios
                        .get(`${URL.BACKEND_URL}user/current`, {
                            headers: {
                                id: id
                            }
                        })
                        .then((res) => (response = res))
                        .catch((err) => console.log(err));
                    const user = response.data;
                    setSession(token);
                    dispatch({
                        type: 'INITIALIZE',
                        payload: {
                            isAuthenticated: true,
                            user
                        }
                    });
                } else {
                    dispatch({
                        type: 'INITIALIZE',
                        payload: {
                            isAuthenticated: false,
                            user: null
                        }
                    });
                }
            } catch (err) {
                console.error(err);
                dispatch({
                    type: 'INITIALIZE',
                    payload: {
                        isAuthenticated: false,
                        user: null
                    }
                });
            }
        };

        initialize();
    }, []);

    const login = async (email, password) => {
        let response;
        await axios
            .post(`${URL.BACKEND_URL}user/login`, { email, password })
            .then((res) => (response = res))
            .catch((err) => console.log(err));
        const user = response.data.user;
        const token = response.data.token;
        setSession(token);
        dispatch({
            type: 'LOGIN',
            payload: {
                isAuthenticated:true,
                user
            }
        });
    };

    const register = async ({ email, password, firstName, lastName }) => {
        try {
            const username = `${firstName} ${lastName}`;
            const data = {
                username: username,
                email: email,
                password: password
            };
            console.log(data, "data on context")
            const response = await axios.post(`${URL.BACKEND_URL}user/signup`, data);
            console.log(response, "resgister response authcontext");
            if (response.status === 201) {
                dispatch({
                    type: 'REGISTER',
                    payload: {
                        isAuthenticated: true,
                        user: response.data
                    }
                });
                window.localStorage.setItem('accessToken', response.accessToken);
                return 1;
            } else {
                return 0;
            }

        } catch (err) {
            console.log(err);
            return err.message;
        }


    };

    const logout = async () => {
        setSession(null);
        dispatch({
            type: 'LOGOUT',
            payload: {
                isAuthenticated: false,
                user: null
            }});
    };


    return (
        <AuthContext.Provider
            value={{
                ...state,
                method: 'local',
                login,
                logout,
                register
            }}
        >
            {children}
        </AuthContext.Provider>
    );
}

export { AuthContext, AuthProvider };
