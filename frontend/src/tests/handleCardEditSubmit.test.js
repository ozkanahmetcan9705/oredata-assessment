import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import handleCardEditSubmit from './path-to-your-file'; // handleCardEditSubmit fonksiyonunuzun bulunduğu dosyanın yolunu buraya ekleyin

describe('handleCardEditSubmit', () => {
    const mock = new MockAdapter(axios);

    afterEach(() => {
        mock.reset();
    });

    it('should make a PUT request with correct data and headers', async () => {
        const user = { _id: 'yourUserId' }; // Test kullanıcı bilgisi
        const cardId = 'yourCardId'; // Test kartı bilgisi
        const updatedTitle = 'newTitle';
        const description = 'newDescription';

        const expectedData = {
            title: updatedTitle,
            description: description,
            cardId
        };

        const expectedHeaders = {
            headers: {
                id: user._id
            }
        };

        mock.onPut(`${URL.BACKEND_URL}cards/update`, expectedData, expectedHeaders).reply(200, { yourResponseData: 'mockedData' });

        // Fonksiyonu çağırın
        await handleCardEditSubmit(cardId, updatedTitle, description, user);

        // Beklenen çağrının yapıldığını kontrol edin
        expect(mock.history.put.length).toBe(1);
        expect(mock.history.put[0].data).toEqual(JSON.stringify(expectedData));
        expect(mock.history.put[0].headers).toEqual(expectedHeaders.headers);
    });

    it('should log the response', async () => {
        const cardId = 'yourCardId'; // Test kartı bilgisi
        const updatedTitle = 'newTitle';
        const description = 'newDescription';

        mock.onPut(`${URL.BACKEND_URL}cards/update`).reply(200, { yourResponseData: 'mockedData' });

        // Fonksiyonu çağırın
        await handleCardEditSubmit(cardId, updatedTitle, description);

        // Console.log çağrısının yapıldığını kontrol edin
        expect(console.log).toHaveBeenCalledWith(expect.objectContaining({ yourResponseData: 'mockedData' }));
    });
});
