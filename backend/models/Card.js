const mongoose = require( "mongoose");
const Schema = mongoose.Schema;

const CardSchema = new Schema({
    cardId:{type:String},
    title:{type:String, required:true},
    column:{type:String, ref:'cards'},
    description:{type:String, default: null},
    createdAt: {
        type:Date,
        default:Date.now()}
});

const Card = mongoose.model('tasks',CardSchema);

module.exports= Card;

