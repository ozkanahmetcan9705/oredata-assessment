const mongoose = require( "mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema({
    username:{type:String, required:true},
    email:{
        type:String,
        required:true,
        unique:true,
        match:/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/

},
    password:{type:String, required:true},
    date:{type:Date, default:Date.now()},
    token:{
        type:String,
        required:false
    },
});

const User = mongoose.model('users',userSchema);

module.exports= User;

