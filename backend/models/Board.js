// models/board.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

if (!mongoose.models.boards) {
  const BoardSchema = new Schema({
    user: {
      type: Schema.Types.ObjectId,
      ref: 'users',
    },
    title: { type: String, required: true },
    columnOrder: [{ type: String, ref: 'columns' }],
    guests: [{
      type: Schema.Types.ObjectId,
      ref: 'users',
      default: null
    }],
    createdAt: {
      type: Date,
      default: Date.now()
    }
  });

  const Board = mongoose.model('boards', BoardSchema);
  module.exports = Board;
} else {
  module.exports = mongoose.model('boards');
}
