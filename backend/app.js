const express = require( "express");

const morgan = require("morgan");
const bodyParser = require( "body-parser");
const cors = require( "cors");
const userRouter = require( "./api/routes/users");
const boardRouter = require ("./api/routes/boards");
const columnRouter = require( "./api/routes/columns");
const cardRouter = require( "./api/routes/cards");

require('dotenv').config();

const app = express();

app.use(morgan('dev'));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, id');
    res.setHeader('Access-Control-Allow-Credentials', true);
    if (req.method === 'OPTIONS') {
        return res.status(200).end();
    }
    next();
});


app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use('/api/user',userRouter);
app.use('/api/boards',boardRouter);
app.use('/api/columns',columnRouter);
app.use('/api/cards',cardRouter);

app.use((req,res,next)=>{
   const error = new Error('no route found ');
   error.status=404;
   next(error);
});

app.use((error,req,res,next)=>{
   res
       .status(error.status || 500)
       .json({
           error:{
               message:error.message
           }})
});


module.exports =app;




