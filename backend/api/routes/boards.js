const express =  require('express');
const checkAuth =  require("../../middleware/check-auth");
const Board =  require("../../models/Board");
const mongoose =  require("mongoose");
const internalErrorResponse =  require("../../utils/internalErrorResponse");


const boardRouter = express.Router();
// route POST /api/boards
// required: authToken, title
// desc Create a new board
// access private
boardRouter.post('/add', (req, res, next) => {
    const title = req.body.title;
    const userID = req.body.userId;
    const now = new Date();
    Board.find()
        .exec()
        .then(board => {
            const newBoard = new Board({
                user: userID,
                title,
                columnOrder: []
            });
            newBoard.save()
                .then(result => res.status(201).json({message:'created a new board',result}))
                .catch(err => res.status(500).json(err));
        })
        .catch(error => internalErrorResponse(error, res))
});

boardRouter.post('/addguest', checkAuth, async (req, res, next) => {
    try {
        const boardId = req.body.boardId;
        const guestId = req.body.guestId;

        // Find the board by its ID
        const board = await Board.findOne({ _id: boardId });

        if (!board) {
            return res.status(404).json({ message: 'Board not found' });
        }

        // Check if guests array is null, initialize it to an empty array if necessary
        const guestsArray = board.guests || [];
        console.log('guestsArray:', guestsArray);

        if (guestsArray.includes(guestId)) {
            return res.status(400).json({ message: 'Guest is already added to the board' });
        }

        guestsArray.push(guestId);
        board.guests = guestsArray;  // Assign the updated array back to the board
        const updatedBoard = await board.save();

        return res.status(200).json({ message: 'Guest added successfully', updatedBoard });

    } catch (error) {
        // Handle errors
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
});




// route PATCH /api/boards
// required: authToken,boardId, payload: destination,source
// desc Reorder columns  in a  board
// access private

boardRouter.patch('/', checkAuth, (req, res) => {
    const { boardId, newColumnOrder } = req.body;
    if(boardId && newColumnOrder)
    {
       Board.findOneAndUpdate({_id:boardId}, { columnOrder: newColumnOrder })
                    .exec()
                    .then((board) => {
                        
                        const updatedColumnOrder = board.columnOrder;
                        
                        res.status(200).json({ message: 'Reorder success', updatedColumnOrder })
                    })
                    .catch(error => internalErrorResponse(error, res))
            


       }
       else {
           return res.status(400).json({message:'required parameters are missing'});
       }


});

// route GET /api/boards/board
// required: authToken
// desc Get a board with id
// access private

boardRouter.get('/board/:boardId', checkAuth, (req, res, next) => {
    const boardId = req.params.boardId;

    // Check if boardId is undefined
    if (!boardId) {
        return res.status(400).json({ message: 'Board ID is undefined' });
    }

    Board.findOne({ _id: boardId })
        .exec()
        .then(board => {
            if (!board) {
                return res.status(404).json({ message: 'Board with given id was not found' });
            }
            return res.status(200).json({ details: board });
        })
        .catch(error => internalErrorResponse(error, res));
});


boardRouter.put('/boardupdate', checkAuth, async (req, res, next) => {
    const { title, boardId } = req.body;

    try {
        const updatedBoard = await Board.findOneAndUpdate(
            { _id: boardId },
            { $set: { title: title } },
            { new: true }
        ).exec();

        if (!updatedBoard) {
            return res.status(404).json({ message: 'Board with given id was not found' });
        }

        return res.status(200).json({ details: updatedBoard });
    } catch (error) {
        internalErrorResponse(error, res);
    }
});


// route GET /api/boards/all
// required: authToken
// desc Get all boards of the user
// access private

boardRouter.get('/all', checkAuth, async (req, res, next) => {
    const userId = req.headers.id;

    try {
        // Find boards where the user is the owner
        const boards = await Board.find({ user: userId })
            .select('columnOrder title _id user guests')
            .exec();

        // Find boards where the user is a guest
        const guestBoards = await Board.find({ guests: { $in: [userId] } })
            .select('columnOrder title _id user guests')
            .exec();

        if (boards.length === 0 && guestBoards.length === 0) {
            return res.status(200).json({ message: 'No boards found for this user', ownerBoards: [], guestBoards: [] });
        }

        return res.status(200).json({ message: 'Success', boards, guestBoards });
    } catch (error) {
        internalErrorResponse(error, res);
    }
});

boardRouter.post('/delete', checkAuth, async (req, res, next) => {
    const boardId = req.body.boardId;
    try {
        await Board.deleteOne({ _id: boardId }).exec();

        return res.status(200).json({ message: 'Success, board is deleted' });
    } catch (error) {
        internalErrorResponse(error, res);
    }
});



module.exports= boardRouter;
