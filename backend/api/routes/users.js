const express = require('express');
const User = require('../../models/User');
const httpStatus = require('http-status');
const validateRegisterInput = require('../../validation/register');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const validateLoginInput = require('../../validation/login');
const checkAuth = require('../../middleware/check-auth');

require('dotenv').config();

const userRouter = express.Router();

// @route POST /api/user/register
// @desc register the user
// @ access Public
userRouter.post('/signup', (req, res, next) => {
  const { errors, isValid } = validateRegisterInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }
  const { email, password, username } = req.body;

  User.findOne({ email })
    .exec()
    .then(user => {
      if (user) {
        res
          .status(409)
          .json({ message: 'User with this email already exists' });
      } else {
        const newUser = new User({
          username,
          email,
          password,
        });

        bcrypt.hash(password, 10).then(hash => {
          newUser.password = hash;
          newUser
            .save()
            .then(user => res.status(201).json(user))
            .catch(error => res.status(500).json(error));
        });
      }
    })
    .catch(err => res.status(500).json(err));
});

// @route POST /api/user/login
// @desc login the user
// @ access Public

userRouter.post('/login', async (req, res, next) => {
  try {
    const { errors, isValid } = validateLoginInput(req.body);
    const APP_SECRET="soup4every1";
    if (!isValid) {
      return res.status(400).json(errors);
    }
    const { email, password } = req.body;
    const user = await User.findOne({ email: req.body.email }).exec();
    if (!user) {
      return res.status(401).json({ message: 'Auth Failed' });
    }
    const result = await bcrypt.compare(password, user.password);
    if (result) {
      let payload = { sub: user.id };
      const token = jwt.sign(payload, APP_SECRET);
      if(token){
        await User.updateOne({_id:user.id},{$set:{token:token}});
        return res.json({
          status: httpStatus.OK,
          message: 'OK',
          token,
          user: {
            id: user._id,
            email: user.email,
            username: user.username,
            token: user.token
          }
        });
      }
    } else {
      res.status(401).json({ message: 'Auth Failed' });
    }
  } catch (e) {
    throw new Error(e.message);
  }
});

// @route DELETE /api/user/:userId
// @desc delete the user with provided user Id
// @ access private TODO make it private
userRouter.delete('/:userId', (req, res, next) => {
  User.findOneAndDelete({ _id: req.params.userId })
    .exec()
    .then(() => res.status(200).json({ message: 'User deleted' }))
    .catch(err => res.status(500).json(err));
});

userRouter.get('/find', async (req, res, next) => {
  const userId = req.headers.id;
  const user = await User.findOne({ _id: userId }).exec();
  if (!user) {
    return res.status(404).json({
      status: httpStatus.NOT_FOUND,
      message: 'User not found',
    });
  }
  return res.json({
    status: httpStatus.OK,
    message: 'OK',
    user: {
      id: user._id,
      email: user.email,
      username: user.username,
      token: user.token
    },
  });
});

userRouter.get('/findAll', async (req, res, next) => {
  try {
    const users = await User.find().exec();
    return res.json(users);
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      status: httpStatus.INTERNAL_SERVER_ERROR,
      message: 'Internal server error',
    });
  }
});


userRouter.get('/current', checkAuth, (req, res, next) => {
  return res.status(200).json(req.user);
});

module.exports = userRouter;
