const mongoose = require('./setup'); // 'test/setup.js' dosyanızın doğru yolunu belirtin
const chai = require('chai');
const chaiHttp = require('chai-http');
const sinon = require('sinon');
const app = require('../app'); // Backend uygulamanızın ana dosyasını import edin
const Column = require('../models/column'); // Column modelini import edin
beforeEach((done) => {
    mongoose.disconnect();
    mongoose.connect('mongodb://localhost:27017/test_database', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
    done();
});
chai.use(chaiHttp);
const expect = chai.expect;

describe('Column Routes', () => {
    describe('GET /api/column/:columnId', () => {
        it('should get a column by ID', (done) => {
            // Mock bir column nesnesi oluşturun
            const mockColumn = {
                _id: 'mockColumnId',
                title: 'Mock Column',
            };

            // Column modelinin findOne metodunu mocklayın
            const findOneStub = sinon.stub(Column, 'findOne');
            findOneStub.withArgs({ _id: 'mockColumnId' }).returns({
                exec: sinon.stub().resolves(mockColumn),
            });

            chai.request(app)
                .get('/api/column/mockColumnId')
                .end((err, res) => {
                    // Mock objesinin döndüğüne dair testler
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('message').equal('Success');
                    expect(res.body).to.have.property('details');
                    expect(res.body.details).to.deep.equal(mockColumn);

                    // findOneStub'ı geri alın
                    findOneStub.restore();
                    done();
                });
        });

        it('should return 404 if column with given ID is not found', (done) => {
            // Column modelinin findOne metodunu mocklayın
            const findOneStub = sinon.stub(Column, 'findOne');
            findOneStub.withArgs({ _id: 'nonexistentColumnId' }).returns({
                exec: sinon.stub().resolves(null),
            });

            chai.request(app)
                .get('/api/column/nonexistentColumnId')
                .end((err, res) => {
                    // 404 durumunu kontrol edin
                    expect(res).to.have.status(404);
                    expect(res.body).to.have.property('message').equal('Column with given id not found');

                    // findOneStub'ı geri alın
                    findOneStub.restore();
                    done();
                });
        });

        it('should handle internal server error', (done) => {
            // Column modelinin findOne metodunu mocklayın ve hata fırlatın
            const findOneStub = sinon.stub(Column, 'findOne');
            findOneStub.withArgs({ _id: 'mockColumnId' }).throws(new Error('Internal Server Error'));

            // internalErrorResponse fonksiyonunu mocklayın
            const internalErrorResponseStub = sinon.stub();
            internalErrorResponseStub.returns({});

            chai.request(app)
                .get('/api/column/mockColumnId')
                .end((err, res) => {
                    // Internal server error durumunu kontrol edin
                    expect(res).to.have.status(500);

                    // findOneStub'ı ve internalErrorResponseStub'ı geri alın
                    findOneStub.restore();
                    internalErrorResponseStub.restore();
                    done();
                });
        });
    });
});
