const mongoose = require('mongoose');

// Mongoose bağlantısını kur
mongoose.connect('mongodb://localhost:27017/test_database', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

// Test süreci boyunca kullanılacak olan mongoose bağlantısını export et
module.exports = mongoose;
