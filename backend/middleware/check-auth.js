const jwt = require('jsonwebtoken');
const User = require('../models/User');

require('dotenv').config();

const checkAuth = (req, res, next) => {
  try {
      /*const APP_SECRET="soup4every1";
    const token = req.headers.authorization.split(' ')[1];
    const decoded = jwt.verify(token, APP_SECRET);
    const { userId } = decoded;*/
      const userId = req.headers.id;

    User.findOne({ _id: userId })
      .exec()
      .then(user => {
        if (!user) {
          return res.status(401).json({ message: 'Auth Failed' });
        }
        req.user = user;
        console.log('auth success');
        return next();
      })
      .catch(error => res.status(500).json(error));
  } catch (error) {
    return res.status(401).json({ message: 'Auth Failed' });
  }
};

module.exports = checkAuth;
