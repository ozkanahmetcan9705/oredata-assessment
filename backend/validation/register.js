const validator = require( "validator");
const {isEmpty} = require( "./is-empty");

 const validateRegisterInput = (data) => {
    let errors = {};
    data.username = !isEmpty(data.username) ? data.username : '';
    data.email = !isEmpty(data.email) ? data.email : '';
    data.password = !isEmpty(data.password) ? data.password : '';
    const re=/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

    if (!validator.isLength(data.username, {min: 2, max: 30})) {
        errors.name = 'Name must be between 2 and 30 characters';
    }
    if (validator.isEmpty(data.username)) {
        errors.name = 'Name field is required';
    }

    if (!re.test(data.email)) {
        errors.email = 'Email is invalid';
    }

    if (validator.isEmpty(data.password)) {
        errors.password = 'Password required';
    }
    if (!validator.isLength(data.password, {min: 4, max: 30})) {
        errors.password = 'Password must be at least 4 characters';
    }

    return {errors, isValid: isEmpty(errors)};
};

 module.exports = validateRegisterInput;