const mongoose = require("mongoose");
const http = require("http");
const app = require("./app");

require("dotenv").config();

const port = 3002;
const server = http.createServer(app);

// Connecting to the DB
mongoose.promise = global.Promise;

mongoose
  .connect("mongodb://127.0.0.1:27017/oredatatask", {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log("Database connected");
  })
  .catch(err => console.log(err));

server.listen(port, () => {
  console.log(`listening on ${port}`);
});
